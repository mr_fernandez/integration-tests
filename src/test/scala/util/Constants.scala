package util

object Constants {
  def baseUrl = sys.props.getOrElse("server", "http://10.207.102.40:7017/rest")
  def crm = baseUrl + "/crm"
  def cases = crm + "/cases"
  def contracts = crm + "/contracts"
  def order = crm + "/order"
  def charsetHeader = "*/*"
  def contentType = "Content-Type"
  def applicationJson = "application/json"
  def error = "ERROR"
  def notificationZeroMessage = "$.notifications[0].message"
  def notificationZeroPath = "$.notifications[0].path"
  def notificationZeroSeverity = "$.notifications[0].severity"
  def payloadZero = "$.payload[0]"
  def payloadOne = "$.payload[1]"
  def payloadTwo = "$.payload[2]"
  def xChannel = "X-CHANNEL"
  def xMipAuthenticatedUser = "X-MIP-Authenticated-User"
  def xRequestSource = "X-Request-Source"
  def test = "test"
  def OK = 200
  def UNAUTHORISED = 401
  def NOT_FOUND = 404
  def UNKNOWN = 422
  def INTERNAL_SERVER_ERROR = 500
}
