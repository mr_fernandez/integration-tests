package contracts

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import util.Constants._

class ContractSimulation extends Simulation {

  val httpProtocol = http.baseUrl(contracts)
    .acceptCharsetHeader(charsetHeader)
    .header(contentType, applicationJson)
    .header(xChannel, test)
    .header(xChannel, test)

  val confirmationNoteNotFound = scenario("confirmationNoteNotFound")
    .exec(http("confirmationNoteNotFound")
      .get("/00882045/confirmationNote")
      .header(xMipAuthenticatedUser, "b00882045")
      .check(status.is(NOT_FOUND))
      .check(jsonPath(notificationZeroPath).is(path("00882045")))
      .check(jsonPath(notificationZeroMessage).is("No AB document found"))
      .check(jsonPath(notificationZeroSeverity).is(error)))

  val confirmationNoteAccessNotAllowed = scenario("confirmationNoteAccessNotAllowed")
    .exec(http("confirmationNoteAccessNotAllowed")
      .get("/8528257/confirmationNote")
      .header(xMipAuthenticatedUser, "x")
      .check(status.is(UNAUTHORISED))
      .check(jsonPath(notificationZeroPath).is(path("8528257")))
      .check(jsonPath(notificationZeroMessage).is("Access not allowed for contract context with contract id: 8528257 and uid: x"))
      .check(jsonPath(notificationZeroSeverity).is(error)))

  val confirmationNoteFound = scenario("confirmationNoteFound")
    .exec(http("confirmationNoteFound")
      .get("/00885373/confirmationNote")
      .header(xMipAuthenticatedUser, "m00885373")
      .check(status.is(OK))
      .check(jsonPath("$.payload.downloadLink").exists))

  // Group all requests
  val allContracts = scenario("allCases").group("allCases") {
    exec(confirmationNoteAccessNotAllowed)
      .exec(confirmationNoteNotFound)
      .exec(confirmationNoteFound)
  }

  setUp(allContracts.inject(atOnceUsers(1)).protocols(httpProtocol))

  private def path(contractId: String): String = {
    val path = "/rest/crm/contracts/".concat(contractId).concat("/confirmationNote")
    path
  }

}
