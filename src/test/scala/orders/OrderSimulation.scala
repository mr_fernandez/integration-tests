package orders

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import util.Constants._

class OrderSimulation extends Simulation {

  val httpProtocol = http.baseUrl(order)
    .acceptCharsetHeader(charsetHeader)
    .header(contentType, applicationJson)
    .header(xChannel, test)
    .header(xChannel, test)
  // Attempt to save order with empty marketing name.
  val emptyMarketingName = unHappyPostScenario(
    "emptyMarketingName",
    "6392073",
    "orderWithEmptyMarketingName")
  // Attempt to save order with empty marketing name.
  val emptyTransactionId= unHappyPostScenario(
    "emptyTransactionId",
    "6392073",
    "orderWithEmptyTransactionId")
  // Attempt to save order with existing transaction id.
  val existingTransactionId = unHappyPostScenario(
    "existingTransactionId",
    "6392073",
    "orderWithExistingTransactionId")
  // Group all requests
  val allOrders: ScenarioBuilder = scenario("allOrders").group("allOrders") {
    exec(emptyMarketingName)
      .exec(emptyTransactionId)
      .exec(existingTransactionId)
  }

  setUp(allOrders.inject(atOnceUsers(1)).protocols(httpProtocol))

  private def unHappyPostScenario(requestName: String, contractId: String, jsonFileName: String): ScenarioBuilder = {
    scenario(requestName)
      .exec(http(requestName)
        .post(patchUrl(contractId))
        .body(RawFileBody(fileName(jsonFileName))).asJson
        .check(status.is(UNKNOWN))
        .check(jsonPath(notificationZeroPath).is(path(contractId)))
        .check(jsonPath(notificationZeroSeverity).is(error)))
  }

  private def fileName(fileName: String): String = {
    val finalFileName = "orders/".concat(fileName).concat(".json")
    finalFileName
  }

  private def patchUrl(contractId: String): String  = {
    val patchUrl = "/".concat(contractId).concat("/save")
    patchUrl
  }

  private def path(contractId: String): String = {
    val path = "/rest/crm/order/".concat(contractId).concat("/save")
    path
  }

}
