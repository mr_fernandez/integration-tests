package cases

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import util.Constants._

import scala.concurrent.duration._

class CaseSimulation extends Simulation {

  val httpProtocol = http.baseUrl(cases)
    .acceptCharsetHeader(charsetHeader)
    .header(contentType, applicationJson)
    .header(xChannel, test)
    .header(xChannel, test)
  // Update delivery address for case.
  val withAddress = happyPatchScenario(
    "withAddress",
    "287178990",
    "deliveryAddress")
  // Update delivery address for case with pickup station.
  val withPickupStation = happyPatchScenario(
    "withPickupStation",
    "287178990",
    "pickupStationAddress")
  // Update delivery address for case with valid address.
  val validAddress = happyPatchScenario(
    "validAddress",
    "286717514",
    "deliveryAddress")
  // Update delivery address for case with invalid address.
  val invalidAddress = unHappyPatchScenario(
    "invalidAddress",
    "286717514",
    "invalidAddress",
    "Unable to update delivery address for case with id 286717514 due to: Fields First name, Address, Street number are missing.")
  // Update delivery address for closed case.
  val closedCase = unHappyPatchScenario(
    "closedCase",
    "286272141",
    "validAddress",
    "Unable to update delivery address for case with id 286272141 due to: Package is already shipped.")
  // Update delivery address for already delivered case.
  val alreadyDelivered = unHappyPatchScenario(
    "alreadyDelivered",
    "285560700",
    "validAddress",
    "Unable to update delivery address for case with id 285560700 due to: Package is already shipped.")
  // Download link
  val downloadLink = scenario("downloadLink")
    .exec(http("downloadLink")
      .get("/note/Endkundenschreiben")
      .queryParam("caseID", "8388497")
      .queryParam("x_type_lvl1", "Endkundenschreiben")
      .check(status.is(OK))
      .check(jsonPath("$.payload.downloadLinkList[0].title").is("Kundenschreiben"))
      .check(jsonPath("$.payload.downloadLinkList[1].title").is("Kundenschreiben")))
  // MMD Question
  val mmdQuestion = scenario("mmdQuestion")
    .exec(http("mmdQuestion")
      .get("/6598816/mmdQuestion")
      .header(xMipAuthenticatedUser, "h6598816")
      .check(status.is(OK))
      .check(jsonPath("$.payload").is("true")))
  // Get open technician
  val openTechnician = scenario("openTechnician")
    .exec(http("openTechnician")
      .get("/8633028/appointments")
      .header(xMipAuthenticatedUser, "a1100618354796")
      .check(status.is(OK)))
  // Get open technician unauthorised
  val openTechnicianUnAuthorised = scenario("openTechnicianUnAuthorised")
    .exec(http("openTechnicianUnAuthorised")
      .get("/8633028/appointments")
      .check(status.is(UNAUTHORISED)))

  val closedStatus = scenario("closedStatus")
    .exec(http("closedStatus")
      .get("/00885392/status")
      .header(xMipAuthenticatedUser, "m00885392")
      .check(status.is(OK))
      .check(jsonPath("$.payload.closedCasesSummary").is("1"))
      .check(jsonPath("$.payload.caseStates[*].state").findAll.in(Seq("Closed", "Closed", "Closed")))
      .check())
  // Group all requests
  val allCases: ScenarioBuilder = scenario("allCases").group("allCases") {
    exec(withAddress)
      .exec(withPickupStation)
      .exec(validAddress)
      .exec(invalidAddress)
      .exec(closedCase)
      .exec(alreadyDelivered)
      .exec(downloadLink)
      .exec(mmdQuestion)
      .exec(openTechnician)
      .exec(openTechnicianUnAuthorised)
      .exec(closedStatus)
  }

  setUp(allCases.inject(atOnceUsers(1)).protocols(httpProtocol))

  private def happyPatchScenario(requestName: String, caseObjid: String, jsonFileName: String): ScenarioBuilder = {
    scenario(requestName)
      .exec(http(requestName)
        .patch(patchUrl(caseObjid))
        .body(RawFileBody(fileName(jsonFileName))).asJson
        .check(status.is(OK)))
  }

  private def unHappyPatchScenario(requestName: String, caseObjid: String, jsonFileName: String, errorMessage: String): ScenarioBuilder = {
    scenario(requestName)
      .exec(http(requestName)
        .patch(patchUrl(caseObjid))
        .body(RawFileBody(fileName(jsonFileName))).asJson
        .check(status.is(INTERNAL_SERVER_ERROR))
        .check(jsonPath(notificationZeroPath).is(path(caseObjid)))
        .check(jsonPath(notificationZeroMessage).is(errorMessage))
        .check(jsonPath(notificationZeroSeverity).is(error)))
  }

  private def fileName(fileName: String): String = {
    val finalFileName = "cases/".concat(fileName).concat(".json")
    finalFileName
  }

  private def patchUrl(caseObjid: String): String  = {
    val patchUrl = "/".concat(caseObjid).concat("/deliveryAddress")
    patchUrl
  }

  private def path(caseObjid: String): String = {
    val path = "/rest/crm/cases/".concat(caseObjid).concat("/deliveryAddress")
    path
  }

}
