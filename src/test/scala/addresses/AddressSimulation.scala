package addresses

import util.Constants.{OK, applicationJson, charsetHeader, contentType, crm, payloadOne, payloadTwo, payloadZero, test, xChannel, xMipAuthenticatedUser, xRequestSource}
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

import scala.concurrent.duration._

class AddressSimulation extends Simulation {

  def payloadZeroObjid = payloadZero.concat(".objid")
  def payloadOneObjid = payloadOne.concat(".objid")
  def payloadZeroSiteObjid = payloadZero.concat(".siteObjid")
  def payloadZeroStreetNumber = payloadZero.concat(".streetNumber")
  def payloadOneSiteObjid = payloadOne.concat(".siteObjid")
  def payloadOneStreetNumber = payloadOne.concat(".streetNumber")
  def payloadOneBuildingPart = payloadOne.concat(".buildingPart")
  def payloadTwoSiteObjid = payloadTwo.concat(".siteObjid")
  def payloadTwoStreetNumber = payloadTwo.concat(".streetNumber")

  val httpProtocol = http.baseUrl(crm)
    .acceptCharsetHeader(charsetHeader)
    .header(contentType, applicationJson)
    .header(xChannel, test)
    .header(xChannel, test)

  val addressesByQuery = scenario("addressByQuery")
    .exec(http("addressByQuery")
      .post("/addresses/streets")
      .body(RawFileBody("addresses/addressQuery.json")).asJson
      .check(status.is(OK))
      .check(jsonPath(payloadZeroSiteObjid).is("5118335"))
      .check(jsonPath(payloadZeroStreetNumber).is("13")))

  val addressesByQueryWithPostCode = scenario("addressesByQueryWithPostCode")
    .exec(http("addressesByQueryWithPostCode")
      .post("/addresses/streets")
      .body(RawFileBody("addresses/addressQueryWithStreetNumber.json")).asJson
      .check(status.is(OK))
      .check(jsonPath(payloadZeroSiteObjid).is("272258826"))
      .check(jsonPath(payloadZeroStreetNumber).is("70/71"))
      .check(jsonPath(payloadOneSiteObjid).is("272311477"))
      .check(jsonPath(payloadOneStreetNumber).is("71"))
      .check(jsonPath(payloadOneBuildingPart).is("HH"))
      .check(jsonPath(payloadTwoSiteObjid).is("272311493"))
      .check(jsonPath(payloadTwoStreetNumber).is("71")))

  val districts = scenario("districts")
    .exec(http("districts")
      .get("/addresses/districts?city=Halle&zipcode=06108")
      .check(status.is(OK)))

  val streetsByPostcode = scenario("streetsByPostcode")
    .exec(http("streetsByPostcode")
      .get("/addresses/06108/streets")
      .check(status.is(OK)))

  val streetsNumbers = scenario("streetsNumbers")
    .exec(http("streetsNumbers")
      .get("/addresses/streetNumbers?city=Halle&street=Adam-Kuckhoff-Str.&zipcode=06108")
      .check(status.is(OK))
      .check(jsonPath(payloadZeroObjid).is("5096746"))
      .check(jsonPath(payloadZeroStreetNumber).is("1"))
      .check(jsonPath(payloadOneObjid).is("271981741"))
      .check(jsonPath(payloadOneStreetNumber).is("11")))

  val updateBillingAddress = scenario("billingAddressUpdate")
    .exec(http("billingAddressUpdate")
      .patch("/addresses/9010575")
      .header(xMipAuthenticatedUser, "y1105595448924")
      .body(RawFileBody("addresses/billingAddressUpdate.json")).asJson
      .check(status.is(OK)))

  val primarySiteAddress = scenario("primarySiteAddress")
    .exec(http("primarySiteAddress")
      .get("/addresses/primary/site/272311493")
      .check(status.is(OK)))

  val allContractAddresses = createContractAddressScenario(
    "allContractAddresses",
    "00882045",
    "all",
    "b00882045")
  val billingContractAddresses = createContractAddressScenario(
    "billingContractAddresses",
    "00882045",
    "billing",
    "b00882045")
  val primaryContractAddresses = createContractAddressScenario(
    "primaryContractAddresses",
    "00882045",
    "primary",
    "b00882045")

  val getAllAddresses: ScenarioBuilder = scenario("allAddresses").group("allAddresses") {
    exec(streetsByPostcode)
      .exec(primarySiteAddress)
      .exec(streetsNumbers)
      .exec(allContractAddresses)
      .exec(billingContractAddresses)
      .exec(primaryContractAddresses)
      .exec(addressesByQuery)
      .exec(updateBillingAddress)
      .exec(districts)
  }

  setUp(getAllAddresses.inject(atOnceUsers(1)).protocols(httpProtocol))

  private def createContractAddressScenario(requestName: String, contractId: String, path: String, userName: String): ScenarioBuilder = {
    val finalPath = "/addresses/contractId/".replace("contractId", contractId).concat("/").concat(path)
    scenario(requestName)
      .exec(http(requestName)
        .get(finalPath)
        .header(xMipAuthenticatedUser, userName)
        .check(status.is(OK))
        .check(bodyString.saveAs("response")))
  }

}
